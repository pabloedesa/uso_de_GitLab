/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_con_git;
import java.util.Scanner;
/**
 *
 * @author pablo
 */
public class Practica_con_git {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int[] listaNumeros = new int[10];
        int suma = 0;
        for (int i = 0; i < 10; i++)
        {
            System.out.println("Introduce el número " + (i+1) + "/10: ");
            listaNumeros[i] = sc.nextInt();
            suma += listaNumeros[i];
            System.out.println("La media de los números introducidos hasta el momento es: " + ((float)suma/(i+1)));
        }
        
        System.out.println("El listado de números introducidos es el siguiente:");
        for (int i : listaNumeros)
            System.out.print(i + " ");
    }
    
}
